# weeestiti

Opensource Mood Dashboard, No tracking, no ads, just feedback.  

## Welcome

First of all, we would like to welcome you and let you know that we are very pleased by the interest you show to the project. Before contributing we encourage you to read the README, Contributing Guide and Code of Conduct.

Feel free to contribute, we would love to hear from you.

## Table of content

1. [Goals](#goals)
2. [Principles](#principles)
3. [Values](#values)
4. [How to contribute](#how-to-contribute)


## Goals

Our goal is to create an opensource mood Dashboard, respectful of user's privacy.
Around this project, we aim to bring together a community of people eager to share, contribute with joy and good humour.


## Principles

  1. No tracking
  2. No ads


## Values

Our values are :
   - Respect (for more informations refer to the [Code of Conduct](CODE-OF-CONDUCT.md))
   - Equality
   - Meritocracy (read the [Contributing Guide](CONTRIBUTING.md))
   - Love


## How to contribute

Please refer to the Contibuting Guide [here](CONTRIBUTING.md)


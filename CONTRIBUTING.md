# Contributing to weeestiti

First of all, thanks for taking the time to contribute !

The following is a set of guidelines for contributing to weeestiti which is hosted on [GitLab](https://gitlab.com/weeestiti/weeestiti).
Feel free to propose changes to this document in a merge request.


## Table of contents

1. [How can I contribute ?](#how-can-i-contribute-?)
    * [Reporting bugs](#reporting-bugs)
    * [Suggesting enhancements](#suggesting-enhancements)
    * [Your first code contribution](#your-first-code-contribution)
    * [Merge request](#merge-request)
2. [Styleguides](#styleguides)
    * [Git commit messages](#guide-commit-messages)


## How can I contribute ?

### Reporting bugs

Bugs are tracked as GitLab issues. After you have determined that your bug isn't already reported, create an issue and provide the following information.

Explain the problem with the maximum of details to help the maintainers reproduce it :
  * **Use a clear and descriptive title** for the issue to identify the problem.
  * **Describe the exact steps which reproduce the problem** in as many details as possible (include screenshots if you can).
  * **Provide specific examples to demonstrate the steps**
  * **Describe the behavior you observed after following the steps**
  * **Explain which behavior you expected to see instead and why**

Provide more context by answering the following questions :
  * **Did the problem start happening recently** (e.g. after updating to a new version of weeestiti) or was it always a problem ?

Include details about your configuration and environment:
  * **Which version of weeestiti are you using ?**
  * **Which OS (name and version) are you using ?**


### Suggesting enhancements

This section guides you through submitting an enhancement suggestion for weeestiti, including completely new features and minor improvements to existing functionalities. Following these guideline helps maintainers and the community understand your suggestion and find related suggestions.

Enhancements suggestions are tracked as Gitlab issues. After you have checked that your suggestion isn't already reported **and** that you are using the **latest version of weeestiti**, create an issue and provide the following information :
  * **Use a clear and descriptive title** for the issue to identify the suggestion.
  * **Provide a step-by-step description of the suggested enhancement** with as many details as possible.
  * **Provide specific examples to demonstrate the steps**, you can include screenshots.
  * **Describe the current behavior** and **explain which behavior you expected to see instead** and why.
  * **Explain why this enhancement would be useful**

### Your first code contribution

First time contributing ? No worries, issues labeled `first-timer only` are here for you.


### Merge request

The process described here has several goals:
  * Maintain weeestiti's quality
  * Fix problems that are important to users
  * Engage the community in working toward the best possible weeestiti
  * Enable sustainable system for weeestiti's maintainers to review contribution

Please follow theses steps to have your contribution considered by the mainteners :
  1. **Identify what you are doing**. Link to the issue you are fixing.
  2. **Description of the changes**. We must understand the design of your change from this description, what the code will do. If not, the merge request will be closed at the maintainer's discretion.
  3. **Release note**. Describe the change in a single line in terms that user can understand. If this change is not notable enough to be included in release notes you may use "Not applicable" or "N/A". This line will be use in weeestiti's release note.
  4. [**Follow the styleguides**](#styleguides)

While the prerequisites above must be satisfied prior having your merge request reviewed, the reviewer(s) may ask you to complete additional design work, tests, or other changes before your merge request can be ultimately accepted.

## Styleguides

### Git commit messages

  * Use the present tense
  * Use the imperative mood
  * Reference issues and merge request liberally after the first line
  * Consider starting the commit message with :
    * feat: for any new features
	* fix: for any bug fixing

Please note that we choose to use the conventional commits, for more informations please refer to the conventional commit [specification](https://www.conventionalcommits.org/en/v1.0.0-beta.4/)
